/**
 * Created by kperez on 24/08/15.
 */
Template.landingCojinAcolchado.onCreated(function () {
    $('.spinner').fadeOut('slow');
});

Template.landingCojinAcolchado.rendered = function () {
    videojs(
        'videopreview',
        {
            "techOrder": ["youtube"],
            "src": 'https://www.youtube.com/watch?v=22Pu2pa6KZE&feature=youtu.be',
            "ytcontrols": false,
            "forceHTML5": true,
            "quality": "1080p",
            "playsInline": true
        },
        function() {
            // This is functionally the same as the previous example.
        });
    $('html').addClass('no-overflow-y');
    $('body').addClass('white');
    $('body, .navbar-header').css('padding-right', '16px');
    var stickyWidth = $(".col-md-3").css("width");
    $("#sticky-box").css("width", stickyWidth);
    $("#sticky-box").css("top", "10px");
    $('.preloader').delay(100).fadeOut('slow');
    $(window).scroll(function () {
        if ($(window).scrollTop() > 200) {
            $('.navbar-default').css("background", "rgba(24, 46, 67, 1)");
        } else {
            $('.navbar-default').css("background", "rgba(0, 0, 0, 0.6)");
        }
    });
}

Template.landingCojinAcolchado.events({
    'click #verGalery': function (event) {
        event.preventDefault();

        var img_inicial = $('.galeria li').children('img')[0];
        var src = $(img_inicial).attr('src');
        var img = '<img src="' + src + '" class="img-responsive" />';
        showGallery(0, img);
    },

    'click .img-galery': function (event) {
        event.preventDefault();
        var src = $(event.target).attr('src');
        console.log(src);
        var img = '<img src="' + src + '" class="img-responsive" />';

        var index = $(event.target).parent('li').index();

        showGallery(index, img);
    }
});

var showGallery = function (index, img) {
    var html = '';
    html += img;
    html += '<div class="btn-gallery" >';
    html += '<a class="controls next" href="' + (index + 2) + '">Siguiente <span aria-hidden="true" class="fui-triangle-right-large"></span></a>';
    html += '<a class="controls previous" href="' + (index) + '"><span aria-hidden="true" class="fui-triangle-left-large"></span> Anterior</a>';
    html += '</div>';

    $('#galeryImgModal').modal();
    $('#galeryImgModal').on('shown.bs.modal', function () {
        $('#galeryImgModal .modal-body').html(html);
        $('a.controls').trigger('click');
    });
    $('#galeryImgModal').on('hidden.bs.modal', function () {
        $('#galeryImgModal .modal-body').html('');
    });
}