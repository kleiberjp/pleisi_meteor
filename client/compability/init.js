/**
 * Created by kperez on 24/08/15.
 */
$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function () {

    $('#videopreview').css({'height': ($("#videopreview").width() * 9 / 16) + 'px'});

}, false);

$(window).load(function () {
    $(".modal").niceScroll({
        scrollspeed: 60,
        mousescrollstep: 40,
        cursorwidth: 15,
        cursorborder: 0,
        cursorcolor: '#303030',
        cursorborderradius: 0,
        autohidemode: false,
        horizrailenabled: false,
        zindex: 1100000
    });
    $("html").niceScroll({
        scrollspeed: 60,
        mousescrollstep: 40,
        cursorwidth: 15,
        cursorborder: 0,
        cursorcolor: '#303030',
        cursorborderradius: 0,
        autohidemode: false,
        horizrailenabled: false,
        zindex: 1000000
    });

    $(window).trigger('resize');

});

$(document).ready(function () {
    var stickySet = false;
    var stickyTop = 0;

    var stickyWidth = $(".col-md-3").css("width");

    $('#videopreview').css({'height': ($("#videopreview").width() * 9 / 16) + 'px'});
    $(window).resize(function () {
        $('#videopreview').css({'height': ($("#videopreview").width() * 9 / 16) + 'px'});
        //if ($(window).width() > 980) {
        //    stickyWidth = $(".col-md-3").css("width");
        //    $("#sticky-box").css("width", stickyWidth);
        //    $("#sticky-box").css("top", "-150px");
        //    $("#sticky-box").css("position", "absolute");
        //    $("#sticky-box").css("z-index", "15");
        //    stickyTop = $("#sticky-box").offset().top;
        //}
    });

    //$(window).scroll(function () {
    //
    //    //if (!stickySet) {
    //    //    stickyTop = $("#sticky-box").offset().top;
    //    //    stickySet = true;
    //    //}
    //
    //    if ($(window).width() > 979) {
    //        if ($(window).scrollTop() > (stickyTop - 80)) {
    //            $('#sticky-box').stickOnScroll({
    //                topOffset: $(".navbar.navbar-default.navbar-fixed-top").outerHeight(),
    //                footerElement: $(".similars"),
    //                bootomOffset: 20,
    //                setParentOnStick: true,
    //                setWidthOnStick: false
    //            });
    //        }
    //    }
    //
    //});

});