/**
 * Created by kperez on 24/08/15.
 */
Template.galery.events({
    'click .controls': function (event) {
        event.preventDefault();
        var index = $(event.target).attr('href');
        var src = $('ul.row li:nth-child(' + index + ') img').attr('src');

        $('.modal-body img').attr('src', src);

        var newPrevIndex = parseInt(index) - 1;
        var newNextIndex = parseInt(newPrevIndex) + 2;

        if ($(event.target).hasClass('previous')) {
            $(event.target).attr('href', newPrevIndex);
            $('a.next').attr('href', newNextIndex);
        } else {
            $(event.target).attr('href', newNextIndex);
            $('a.previous').attr('href', newPrevIndex);
        }

        var total = $('ul.row li').length + 1;
        //hide next button
        if (total === newNextIndex) {
            $('a.next').hide();
        } else {
            $('a.next').show();
        }
        //hide previous button
        if (newPrevIndex === 0) {
            $('a.previous').hide();
        } else {
            $('a.previous').show();
        }

        return false;
    },
});