/**
 * Created by kperez on 25/08/15.
 */
Template.formularioRegistro.events({
    'submit #signUpForm': function(e, t) {
        e.preventDefault();

        $("#signUpForm [type='submit']").attr('disabled', 'disabled').html("Registrando ...");

        var email = event.target.email.value,
            password = event.target.pswd.value,
            name = event.target.name.value;
        var referred = Router.current().params.query.referred ? Router.current().params.query.referred : "";
        return Accounts.createUser({email: email, password: password, profile: { email: email, name: name, refered: referred }}, function(err) {
            if (err) {
                if (err.message === 'Email already exists. [403]') {
                    console.log('We are sorry but this email is already used.');
                } else {
                    console.log('We are sorry but something went wrong.');
                }
            } else {
                $("#signUpForm [type='submit']").removeAttr('disabled').html("Registrarme");
                $('#successModal').modal('show');
            }
        });

    }
});