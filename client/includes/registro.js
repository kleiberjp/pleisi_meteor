/**
 * Created by kperez on 25/08/15.
 */
Template.registro.events({
    'click .btn-facebook': function (event) {
        event.preventDefault();
        var referred = Router.current().params.query.referred ? Router.current().params.query.referred : "";
        return Meteor.loginWithFacebook({
            requestPermissions: ['email'],
        }, function(error) {
            if (error) {
                return console.log(error.reason);
            }else{
                Meteor.users.update( Meteor.userId() , {$set: {"profile.refered": referred}})
                $('#successModal').modal('show');
            }
        });
    },
});