/**
 * Created by kperez on 24/08/15.
 */
Router.configure({
    layoutTemplate: 'layout',
    loadingtemplate: 'preloader',
    trackPageView: true,
    waitOn: function(){ return Meteor.subscribe('user'); }
});


Router.route('/', { name: 'home'});

Router.route('/classes/pic_grat', { name: 'landingCojinAcolchado'});
