/**
 * Created by kperez on 25/08/15.
 */
Meteor.publish("user", function () {
    if (this.userId) {
        return Meteor.users.find({_id: this.userId},
            {fields: {
                'services': 1,
                'profile': 1,
            }});
    } else {
        this.ready();
    }
});