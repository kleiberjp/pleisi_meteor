/**
 * Created by kperez on 24/08/15.
 */
var determineEmail;

Accounts.onCreateUser(function(options, user) {
    var userData;
    userData = {
        email: determineEmail(user),
        name: options.profile ? options.profile.name : ""
    };

    if (userData.email !== null) {
        //Aqui mail de bienvenida
        //Meteor.call('sendWelcomeEmail', userData, function(error) {
        //    if (error) {
        //        return console.log(error);
        //    }
        //});
    }
    if (options.profile) {
        user.profile = options.profile;
        if(user.services.facebook) user.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
        user.services = user.services;
    }
    return user;
});

determineEmail = function(user) {
    var emailAddress, services;
    if (user.emails) {
        return emailAddress = user.emails[0].address;
    } else if (user.services) {
        services = user.services;
        return emailAddress = (function() {
            switch (false) {
                case !services.facebook:
                    return services.facebook.email;
                default:
                    return null;
            }
        })();
    } else {
        return null;
    }
};